class Car {
  static list = [];

  static init(cars) {
    this.list = cars.map((i) => new this(i));
  }

  constructor({
    id,
    plate,
    manufacture,
    model,
    image,
    rentPerDay,
    capacity,
    description,
    transmission,
    available,
    type,
    year,
    options,
    specs,
    availableAt,
  }) {
    this.id = id;
    this.plate = plate;
    this.manufacture = manufacture;
    this.model = model;
    this.image = image;
    this.rentPerDay = rentPerDay;
    this.capacity = capacity;
    this.description = description;
    this.transmission = transmission;
    this.available = available;
    this.type = type;
    this.year = year;
    this.options = options;
    this.specs = specs;
    this.availableAt = availableAt;
  }

  render() {
    return `
      <div class="card">
        <img src="${this.image}" class="card-img-top" alt="..." />
        <div class="card-body">
          <p>${this.manufacture} / ${this.type}</p>
          <h5 class="card-title">Rp ${this.rentPerDay} / hari</h5>
          <p class="card-text">${this.description}</p>
          <div class="statusM">
            <div class="icon">
              <i class="fa-solid fa-user-group"></i>
            </div>
            <p>${this.capacity} Orang</p>
          </div>
          <div class="statusM">
            <div class="icon">
              <i class="fa-solid fa-gear"></i>
            </div>
            <p>${this.transmission}</p>
          </div>
          <div class="statusM">
            <div class="icon">
              <i class="fa-solid fa-calendar"></i>
            </div>
            <p>${this.year}</p>
          </div>
          <button class="btn btlist" type="submit">Pilih Mobil</button>
        </div>
      </div>
    `;
  }
}

{
  /* <p>id: <b>${this.id}</b></p>
<p>plate: <b>${this.plate}</b></p>
<p>manufacture: <b>${this.manufacture}</b></p>
<p>model: <b>${this.model}</b></p>
<p>available at: <b>${this.availableAt}</b></p>
<img src="${this.image}" alt="${this.manufacture}" width="64px"></img> */
}
