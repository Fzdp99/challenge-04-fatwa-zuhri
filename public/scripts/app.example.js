class App {
  constructor() {
    // this.clearButton = document.getElementById("clear-btn");
    this.loadButton = document.getElementById("load-btn");
    this.carContainerElement = document.getElementById("cars-container");
  }

  async init() {
    await this.load();

    // Register click listener
    // this.clearButton.onclick = this.clear;
    // this.loadButton.onclick = this.run;

    this.loadButton.addEventListener("click", () => {
      this.clear();
      this.run();
    });
  }

  run = () => {
    const typeD = document.getElementById("typedriver").value;
    const jumlahP = document.getElementById("jumlahpenumpang").value;
    const tgl = document.getElementById("tanggal").value;
    const waktu = document.getElementById("waktuambil").value;

    const wt = parseInt(waktu);

    const tahunInput = tgl.slice(0, 4);
    const tahunInputINT = parseInt(tahunInput);

    let cardriver = "";
    if (typeD == 1) {
      cardriver = true;
    } else if (typeD == 2) {
      cardriver = false;
    } else {
      cardriver = "bebas";
    }

    Car.list.forEach((car) => {
      // const node = document.createElement("div");
      // node.innerHTML = car.render();
      // this.carContainerElement.appendChild(node)

      let datecar = car.availableAt.toString();

      const jamcar = datecar.slice(16, 18);
      const jamcarINT = parseInt(jamcar);

      const tahuncar = datecar.slice(11, 15);
      const tahuncarINT = parseInt(tahuncar);

      let newjamcarINT = null;
      if (jamcarINT > 12) {
        newjamcarINT = jamcarINT - 12;
      } else {
        newjamcarINT = jamcarINT;
      }
      // ==================================================================
      if (wt <= newjamcarINT) {
        if (tahunInputINT >= tahuncarINT) {
          if (car.available == cardriver) {
            if (car.capacity >= jumlahP) {
              const node = document.createElement("div");
              node.setAttribute("align", "center");
              node.setAttribute("class", "col-sm-4 mt-5");
              node.innerHTML = car.render();
              this.carContainerElement.appendChild(node);
            }
          } else if (cardriver == "bebas") {
            if (car.capacity >= jumlahP) {
              const node = document.createElement("div");
              node.setAttribute("align", "center");
              node.setAttribute("class", "col-sm-4 mt-5");
              node.innerHTML = car.render();
              this.carContainerElement.appendChild(node);
            }
          }
        } else if (tgl == "") {
          if (car.available == cardriver) {
            if (car.capacity >= jumlahP) {
              const node = document.createElement("div");
              node.setAttribute("align", "center");
              node.setAttribute("class", "col-sm-4 mt-5");
              node.innerHTML = car.render();
              this.carContainerElement.appendChild(node);
            }
          } else if (cardriver == "bebas") {
            if (car.capacity >= jumlahP) {
              const node = document.createElement("div");
              node.setAttribute("align", "center");
              node.setAttribute("class", "col-sm-4 mt-5");
              node.innerHTML = car.render();
              this.carContainerElement.appendChild(node);
            }
          }
        }
      } else if (waktu == "Pilih Waktu") {
        if (tahunInputINT >= tahuncarINT) {
          if (car.available == cardriver) {
            if (car.capacity >= jumlahP) {
              const node = document.createElement("div");
              node.setAttribute("align", "center");
              node.setAttribute("class", "col-sm-4 mt-5");
              node.innerHTML = car.render();
              this.carContainerElement.appendChild(node);
            }
          } else if (cardriver == "bebas") {
            if (car.capacity >= jumlahP) {
              const node = document.createElement("div");
              node.setAttribute("align", "center");
              node.setAttribute("class", "col-sm-4 mt-5");
              node.innerHTML = car.render();
              this.carContainerElement.appendChild(node);
            }
          }
        } else if (tgl == "") {
          if (car.available == cardriver) {
            if (car.capacity >= jumlahP) {
              const node = document.createElement("div");
              node.setAttribute("align", "center");
              node.setAttribute("class", "col-sm-4 mt-5");
              node.innerHTML = car.render();
              this.carContainerElement.appendChild(node);
            }
          } else if (cardriver == "bebas") {
            if (car.capacity >= jumlahP) {
              const node = document.createElement("div");
              node.setAttribute("align", "center");
              node.setAttribute("class", "col-sm-4 mt-5");
              node.innerHTML = car.render();
              this.carContainerElement.appendChild(node);
            }
          }
        }
      }
      // ================================================================
    });
  };

  async load() {
    const cars = await Binar.listCars();
    Car.init(cars);
  }

  clear = () => {
    let child = this.carContainerElement.firstElementChild;

    while (child) {
      child.remove();
      child = this.carContainerElement.firstElementChild;
    }
  };
}
